# Community Cage Infrastructure DNS

## Introduction

We offer a range of full-lifecycle services for open source community projects that are strategic to Red Hat. Services range from managed hosting to server colocation (ping/power/pipe/racks) in multiple data centers. We also provide project planning & execution, budget planning, and purchasing support for tenants.

This is a physical co-location, managed + virtual services including private cloud, and public cloud offering from the Open Source Program Office (OSPO) working with Regional IT, ACS, and PnT DevOps.

This repository contains DNS zones for managed tenants, including OSCI itself. This is meant to replace the deprecated RedHat dnsmaps system for communities in a more modern and open way. Tenants can control their own repository, access and workflow, or can let us manage it for them here.

## Implementation

The system uses the [dns4tenants role](https://gitlab.com/osci/ansible-role-dns4tenants) to fetch zone updates in tenants' repos and publish them on our infrastructure.

## CI

Our CI checked the zone validity but the serial bump check is left to the dns4tenants script.

The `placeholders` is used to install mock include files in the directory for primary zones (as defined by the `bind9` Ansible role) in order to allow validation when zone bits are dynamically generated.

